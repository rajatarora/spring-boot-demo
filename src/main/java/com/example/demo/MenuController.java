package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuController {
	
	@Autowired
	private MenuService menuService;
	
	@RequestMapping(value="/menu", method=RequestMethod.GET)
	public List<MenuItem> getAllItems() {
		
		return menuService.getMenu();
	}
	
	
	@RequestMapping(value="/menu", method=RequestMethod.PUT)
	public void addMenuItem(@RequestBody MenuItem menuItem) {
		menuService.addMenuItem(menuItem);
	}
	
	@RequestMapping(value="/menu", method=RequestMethod.DELETE)
	public void deleteMenuItem(@RequestBody MenuItem menuItem) {
		menuService.deleteMenuItem(menuItem);
	}
}
