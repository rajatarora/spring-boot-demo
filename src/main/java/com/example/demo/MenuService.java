package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuService {
	
	@Autowired
	private MenuRepository menuRepository;

	private List<MenuItem> menu;
	
	public MenuService() {
//		MenuItem wednesday = new MenuItem("Biryani", "Wednesday");
//		MenuItem thursday = new MenuItem("Mutton", "Thursday");
//		MenuItem friday = new MenuItem("Dosa", "Friday");
//		
//		this.menu = new ArrayList<>();
//		this.menu.add(wednesday);
//		this.menu.add(thursday);
//		this.menu.add(friday);
	}
	
	public List<MenuItem> getMenu() {
		List<MenuItem> menu = new ArrayList<>();
		menuRepository.findAll().forEach(menu::add);
		return menu;
	}
	
	public void addMenuItem(MenuItem menuItem) {
		menuRepository.save(menuItem);
	}
	
	public void deleteMenuItem(MenuItem menuItem) {
		this.menu.remove(menuItem);
	}
	
}
