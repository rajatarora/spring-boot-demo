package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MenuItem {
	
	@Id
	private String name;
	
	private String weekday;
	
	public MenuItem() {
		
	}
	
	public MenuItem(String name, String weekday) {
		this.name = name;
		this.weekday = weekday;
	}
	
	public String getName() {
		return name;
	}
	public String getWeekday() {
		return weekday;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((weekday == null) ? 0 : weekday.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MenuItem other = (MenuItem) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (weekday == null) {
			if (other.weekday != null)
				return false;
		} else if (!weekday.equals(other.weekday))
			return false;
		return true;
	}
	
	

}
