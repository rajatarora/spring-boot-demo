package com.example.demo;

import org.springframework.data.repository.CrudRepository;

public interface MenuRepository extends CrudRepository<MenuItem, String> {
	
	public MenuItem findByName(String name);

}
